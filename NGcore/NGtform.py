# -*- coding: utf-8 -*-
# Copyright 2019 Ocean Exploration Trust & Ian Vaughn
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


import math
import numpy as np

class Point(object):
    ''' A point.  To be transformed, etc '''
    
    def __init__(self, *raw):
        if raw:
            ret = np.matrix(raw[0])
            
            if ret.shape[1] > ret.shape[0]:
                ret = ret.transpose()
                
            if ret.shape[0] == 3:
                self._arr = np.matrix(np.ones((4,1)))
                self._arr[0:3,0] = ret
            elif ret.shape[0] == 4:
                self._arr = ret
                self._arr = self._arr / self._arr[3]
            else:
                raise ValueError('Input vector MUST have 3 or 4 elements!')
            
            if ret.shape[1] != 1:
                raise ValueError('Input vector MUST be a vector!')
            
        else:
            self._arr = np.matrix([[0],[0],[0],[1]] )
            
    def getXYZ(self):
        ''' Return the point's XYZ coordinates as a 3-vector '''
        return np.array(self._arr, copy=True)[0:3,0]
        
        
class Transform(object):
    ''' A 3DOF rotation + translation transform.  Either may be set to 0'''
    
    def __init__(self):
        ''' Create a new transformation.  If no options given, returns the identity'''
        self._mat = np.matrix(np.identity(4))
        
    @staticmethod
    def Identity():
        return Transform()
    
    def tform(self, pt):
        ''' Transform a point '''

        # Mupltiply        
        return Point(self._mat * pt._arr)
        
    def dot(self, rhs):
        ''' Combine two transforms as (this)*(rhs). That is, apply this transform AFTER the argument'''
        ret = Transform().Identity()
        
        # Something keeps making these arrays.  DAMN YOU, VILE NUMPY!
        ret._mat = np.matrix(self._mat) * np.matrix(rhs._mat)
        
        return ret
        
    def setTrans(self, xyz):
        ''' Set the translation component '''
        
        
        t = np.matrix(xyz)
        if t.shape[0] < t.shape[1]:
            t = t.transpose()
        
        if t.shape[1] != 1:
            raise ValueError('xyz must be 3x1 or 1x3!')
        if t.shape[0] != 3:
            raise ValueError('xyz must be 3x1 or 1x3!')            

        self._mat[0:3,3] = t
        
        
    def getTrans(self):
        return np.copy(self._mat[0:3,3])

    @staticmethod
    def Translation(xyz):
        ''' Define a transform that is a simple translation '''
        ret = Transform()
        ret.setTrans(xyz)
        return ret
        
    @staticmethod
    def RotXdeg(ang):
        ''' Define a rotation about the x-axis (in degrees) '''
        return Transform.RotXrad(ang*math.pi/180.0)
        
    @staticmethod
    def RotXrad(ang):
        ''' Define a rotation about the x-axis (in radians) '''
        ret = Transform.Identity()
        
        ret._mat[1,1] = math.cos(ang)
        ret._mat[2,2] = ret._mat[1,1]
        ret._mat[1,2] = -math.sin(ang)
        ret._mat[2,1] = -ret._mat[1,2]
        
        return ret
        
    @staticmethod
    def RotYdeg(ang):
        ''' Define a rotation about the y-axis (in degrees) '''
        return Transform.RotYrad(ang*math.pi/180.0)
        
    @staticmethod
    def RotYrad(ang):
        ''' Define a rotation about the x-axis (in radians) '''
        ret = Transform.Identity()
        
        ret._mat[0,0] = math.cos(ang)
        ret._mat[2,2] = ret._mat[0,0]
        ret._mat[0,2] = -math.sin(ang)
        ret._mat[2,0] = -ret._mat[0,2]
        
        return ret
        
    @staticmethod
    def RotZdeg(ang):
        ''' Define a rotation about the y-axis (in degrees) '''
        return Transform.RotZrad(ang*math.pi/180.0)
        
    @staticmethod
    def RotZrad(ang):
        ''' Define a rotation about the x-axis (in radians) '''
        ret = Transform.Identity()
        
        ret._mat[0,0] = math.cos(ang)
        ret._mat[1,1] = ret._mat[0,0]
        ret._mat[0,1] = -math.sin(ang)
        ret._mat[1,0] = -ret._mat[0,1]
        
        return ret      
            
    @staticmethod
    def RPHfossenDeg(rph):
        ''' Define a 3DOF rotation matrix using Fossen's rph notation.  
        This is the same notation Louis Whitcomb and Ryan Eustace used in
        their matlab scripts, and therefore the same that Chris Roman uses
        in all his work.  And this library was written by Chris's student.
        
        Uses angles in degrees'''
        
        return Transform.RPHfossenRad(np.array(rph,dtype=float) * (math.pi/180))

    @staticmethod
    def RPHfossenRad(rph):
        ''' Define a 3DOF rotation matrix using Fossen's rph notation.  
        This is the same notation Louis Whitcomb and Ryan Eustace used in
        their matlab scripts, and therefore the same that Chris Roman uses
        in all his work.  And this library was written by Chris's student.
        
        Uses angles in radians'''
        
        yaw = Transform.RotZrad(rph[2])
        pitch = Transform.RotYrad(-rph[1])
        roll = Transform.RotXrad(rph[0])
    
        ret = Transform()
        # This is an artifact from the matlab implementation, which defined
        # rotz and rotx backwards.  I find my approach saner.
        #ret._mat = (  np.matrix(yaw._mat).transpose()
        #            * np.matrix(pitch._mat).transpose()
        #            * np.matrix(roll._mat).transpose() )
        ret._mat = (  np.matrix(yaw._mat)
                    * np.matrix(pitch._mat)
                    * np.matrix(roll._mat) )
            
        return ret
        
    @staticmethod
    def XYZRPHfossenDeg(xyzrph):
        ''' Build a transform from rotation + translation
        using the fossen RPH convention (its weird).  Remember that +z is 
        down!
        
        Angles in degrees'''
        xyzrph_rad = np.array(xyzrph,dtype=float).copy()
        #xyzrph_rad = np.array(xyzrph).copy()
        xyzrph_rad[3:6] *= (math.pi/180.0)
        return Transform.XYZRPHfossenRad(xyzrph_rad)
        
    @staticmethod
    def XYZRPHfossenRad(xyzrph):
        ''' Build a transform from rotation + translation
        using the fossen RPH convention (its weird).  Remember that +z is 
        down!
        
        Angles in radians'''
        
        ret = Transform.RPHfossenRad(xyzrph[3:6])
        ret.setTrans(xyzrph[0:3])
        
        return ret
            
class Pose(object):
    ''' A Pose.  To be transformed, etc '''
    
    def __init__(self, *raw):
        if raw:
            ret = np.matrix(raw[0])
            
            if ret.shape[1] > ret.shape[0]:
                ret = ret.transpose()
                
            if ret.shape[0] == 6:
                self._arr = np.matrix(np.zeros((6,1)))
                self._arr[0:6,0] = ret
            
            if ret.shape[1] != 1:
                raise ValueError('Input vector MUST be a vector!')
            
        else:
            self._arr = np.matrix([[0],[0],[0],[0],[0],[0]] )
            
    def getXYZ(self):
        ''' Return the point's XYZ coordinates as a 3-vector '''
        return np.array(self._arr, copy=True)[0:3,0]
        
    def getXYZRPH(self):
        ''' Return the point's XYZRPH as a 6-vector '''
        return np.array(self._arr, copy=True)[0:6,0]

    def getRPH(self):
        ''' Return the point's angles as a 3-vector '''
        return np.array(self._arr, copy=True)[3:6,0]

    @staticmethod
    def Identity():
        return Pose()
        
    def oplus(self, other, jacobian=False):
        ''' The head-to-tail operation.  
   [X_ik,J_PLUS] = X_ij.oplus(X_jk) returns the coordinate
   frame composition of k to j and j to i such that:
   X_jk is the 6 DOF representation of frame k w.r.t. frame j.
   X_ij is the 6 DOF representation of frame j w.r.t. frame i.
   X_ik is the 6 DOF representation of frame k w.r.t. frame i.
   J_PLUS is the Jacobian of the composition operation
   i.e. J_PLUS = d(X_ik)/d(X_ij,X_jk) evaluated at X_ij, X_jk.

   X_ik = X_ij (+) X_jk
   X_ik = [x,y,z,r,p,h]'

   The above notation and associated Jacobian are based upon*:
   R. Smith, M. Self, and P. Cheeseman.  "Estimating Uncertain
   Spatial Relationships in Robotics".
   
   *Note: my XYZ Euler angle convention follows Fossen's convention
   which is different than the SSC's.  My RPH are HPR in SSC's
   notation, therefore the Jacobian I return is a permutation of the
   Jacobian given in the SSC appendix.

   Ex:  transformation from vehicle pose to camera pose
   % x_lv is the vehicle pose in the local-level frame with covariance P_lv
   x_lv = rand(6,1);
   P_lv = rand(6);

   % x_vc is the static pose of the camera in the vehicle frame 
   % with covariance P_vc
   x_vc = [1.4,0,0,0,0,pi/2]';
   P_vc = zeros(6);

   % x_lc is the pose of the camera in the local-level frame and 
   % P_lc is the first order covariance
   [x_lc,Jplus] = head2tail(x_lv,x_vc);
   P_lc = Jplus*[P_lv, zeros(6); zeros(6), P_vc]*Jplus';

-----------------------------------------------------------------
    History:
    Date            Who         What
    -----------     -------     -----------------------------
    11-10-2003      rme         Created and written.
    11-15-2003      rme         Changed K_1(3,2) & K_2(3,2) to use tan(p_3)
    01-20-2015      jisv        Adapted to python from Ryan Eustice's matlab stuff'''



        # extract pose elements
        x1 = self._arr[0,0]
        y1 = self._arr[1,0]
        z1 = self._arr[2,0]
        r1 = self._arr[3,0]
        p1 = self._arr[4,0]
        h1 = self._arr[5,0]
        R1 = Transform.RPHfossenRad(self._arr[3:6,0])._mat[0:3,0:3]

        x2 = other._arr[0,0]
        y2 = other._arr[1,0]
        z2 = other._arr[2,0]
        r2 = other._arr[3,0]
        p2 = other._arr[4,0]
        h2 = other._arr[5,0]
        R2 = Transform.RPHfossenRad(other._arr[3:6,0])._mat[0:3,0:3]

        # translation component
        X3 = np.zeros(6)
        X3[0:3] = np.squeeze(R1.dot(other._arr[0:3,0]) + self._arr[0:3,0])
        x3 = X3[0]
        y3 = X3[1]
        z3 = X3[2]

        # angle component
        R3 = R1.dot(R2)
        h3 = math.atan2(R3[1,0], R3[0,0]);
        p3 = math.atan2(-R3[2,0], R3[0,0]*math.cos(h3) + R3[1,0]*math.sin(h3));
        r3 = math.atan2(R3[0,2]*math.sin(h3) - R3[1,2]*math.cos(h3), -R3[0,1]*math.sin(h3) + R3[1,1]*math.cos(h3));
        X3[3] = r3
        X3[4] = p3
        X3[5] = h3

        if not jacobian:
            return Pose(X3)

        # compute the Jacobian
        # M
        M      =  np.zeros((3,3));
        M[0,0] =  R1[0,2]*y2 - R1[0,1]*z2;
        M[0,1] =  (z3-z1)*math.cos(h1);
        M[0,2] = -(y3-y1);
        M[1,0] =  R1[1,2]*y2 - R1[1,1]*z2;
        M[1,1] =  (z3-z1)*math.sin(h1);
        M[1,2] =  x3-x1;
        M[2,0] =  R1[2,2]*y2-R1[2,1]*z2;
        M[2,1] = -x2*math.cos(p1)-y2*math.sin(p1)*math.sin(r1)-z2*math.sin(p1)*math.cos(r1);
        M[2,2] =  0;

        # K_1
        K1      =  np.zeros((3,3));
        K1[0,0] =  (math.cos(p1)*math.cos(h3-h1))/math.cos(p3);
        K1[0,1] =  (math.sin(h3-h1))/math.cos(p3);
        K1[0,2] =  0;
        K1[1,0] = -math.cos(p1)*math.sin(h3-h1);
        K1[1,1] =  math.cos(h3-h1);
        K1[1,2] =  0;
        K1[2,0] =  (R2[0,1]*math.sin(r3) + R2[0,2]*math.cos(r3))/math.cos(p3);
        K1[2,1] =  math.tan(p3)*math.sin(h3-h1);
        K1[2,2] =  1;

        # K_2
        K2      =  np.zeros((3,3));
        K2[0,0] =  1;
        K2[0,1] =  math.tan(p3)*math.sin(r3-r2);
        K2[0,2] =  (R1[0,2]*math.cos(h3)+R1[1,2]*math.sin(h3))/math.cos(p3);
        K2[1,0] =  0;
        K2[1,1] =  math.cos(r3-r2);
        K2[1,2] = -math.cos(p2)*math.sin(r3-r2);
        K2[2,0] =  0;
        K2[2,1] =  math.sin(r3-r2)/math.cos(p3);
        K2[2,2] =  math.cos(p2)*math.cos(r3-r2)/math.cos(p3);

        # Jacobian
        Jplus = np.zeros((6,12))
        Jplus[0:3,0:3]  = np.eye(3)
        Jplus[0:3,3:6]  = M
        Jplus[0:3,6:9]  = R1
        #Jplus[0:3,9:12] = zeros

        #Jplus[3:6,0:3]  = zeros
        Jplus[3:6,3:6]  = K1
        #Jplus[3:6,6:9]  = zeros
        Jplus[3:6,9:12] = K2

        return Pose(X3), np.matrix(Jplus)

