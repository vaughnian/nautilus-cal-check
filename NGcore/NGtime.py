# -*- coding: utf-8 -*-
#!/usr/bin/env python3

from datetime import datetime
from datetime import timedelta
import calendar
import math
import os.path

class NGtimestamp(datetime):
    '''Timestamp class for bathy3 / NextGen processing stuff.  
    Eases conversion to/from ISO8601 strings, utime, and rovtime (seconds)'''
    
    def __init__(self, *args, **kwargs):
        '''Initialize by calling the datetime constructor'''
        #... ok, I guess python3 really is better.
        super().__init__()
        
    def __float__(self):
        '''Return the date as decimal seconds since 1 Jan 1970 UTC'''
        return self.toUnix()
        
    def __int__(self):
        '''Return the date as integer microseconds since 1 Jan 1970 UTC'''
        return self.toUtime()
        
    def toUnix(self):
        '''Convert to decimal seconds since 1 Jan 1970 UTC'''
        return calendar.timegm(self.timetuple()) + (self.microsecond/1.0e6)
        
    def toUtime(self):
        '''Conver to microseconds since 1 Jan 1970 UTC'''
        return int(round(self.toUnix()*1.0e6))
        
    def __add__(self, dt):
        return NGtimestamp.fromDatetime(super(NGtimestamp, self).__add__(dt))

    def __sub__(self, dt):
        return NGtimestamp.fromDatetime(super(NGtimestamp, self).__sub__(dt))

    def __ladd__(self, dt):
        return NGtimestamp.fromDatetime(super(NGtimestamp, self).__ladd__(dt))

    def __lsub__(self, dt):
        return NGtimestamp.fromDatetime(super(NGtimestamp, self).__lsub__(dt))

    def applyGPStime(self, gpst):
        ''' Apply a GPS time in seconds since midnight and use it to replace the time while leaving date intact.  Rollover intelligently at midnight '''
        dt = math.fmod(gpst - self.toUnix(), 24.0*3600.0)
        # wrap around in the right place
        if dt < -12.0*3600.0:
            dt += (24.0*3600.0)
        newUnixtime = self.toUnix() + dt

        return NGtimestamp.fromUnix(newUnixtime)

    @staticmethod
    def fromUnix(tstamp):
        ''' Convert from a unix timestamp (assumed UTC) to this '''
        return NGtimestamp.utcfromtimestamp(tstamp)
        
    @staticmethod
    def fromUtime(tstamp):
        ''' Convert from a utime (microseconds, assumed UTC)'''
        return NGtimestamp.fromUnix(float(tstamp)/1.0e6)
    
    @staticmethod
    def fromLog(tstampStr):
        '''Parse a ISO8601 timestamp like those recorded by Ethan's updated DSLOG'''
        # TODO Add timestamp awareness!
        return NGtimestamp.strptime(tstampStr, '%Y-%m-%dT%H:%M:%SZ')
        
    @staticmethod
    def fromDsLog(tstampStr):
        '''Parse a ISO8601 timestamp like those recorded by Ethan's updated DSLOG, but include DsLog-specific bug workarounds (the 60-second bug)'''
        # TODO Add timestamp awareness!

        datepart = tstampStr.strip()[0:16]
        seconds  = tstampStr.strip()[17:-1] # chop the Z

        # Actually parse it
        t = datetime.strptime(datepart, '%Y-%m-%dT%H:%M')
        tstamp = calendar.timegm(t.timetuple())

        return NGtimestamp.fromUnix(tstamp + float(seconds))

    @staticmethod
    def fromOldDsLog(tstampStr):
        '''Parse a ISO8601 timestamp like those recorded by Ethan's updated DSLOG, but include DsLog-specific bug workarounds (the 60-second bug)'''
        # TODO Add timestamp awareness!

        datepart = tstampStr.strip()[0:16]
        seconds  = tstampStr.strip()[17:-1] # chop the Z

        # Actually parse it
        t = datetime.strptime(datepart, '%Y-%m-%d %H:%M')
        tstamp = calendar.timegm(t.timetuple())

        return NGtimestamp.fromUnix(tstamp + float(seconds))


    @staticmethod
    def fromDatetime(dt):
        if isinstance(dt, timedelta):
            return dt
        return NGtimestamp(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, dt.microsecond, dt.tzinfo)

    @staticmethod
    def fromDsLogFilename(dslogFilename):
        ''' Parse a dslog filename into a time for start-of-file '''
        parts = os.path.basename(dslogFilename).split('.', 1)
        return NGtimestamp.strptime(parts[0], '%Y%m%d_%H%M')
