#!/usr/bin/python3
#
# Copyright 2019 Ocean Exploration Trust & Ian Vaughn
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


# Convience classes to parse DSLOG files

import glob
import os
import datetime
import copy
import json

from . import NGtime
from . import NGnmea

class Dslog(object):
    def __init__(self):
        pass

    def parseDir(self, dirname, globPattern, startT=None, endT=None):
        ''' Parse all files that match globPattern in directory dirname
        Optionally, specify a start and end time.  Functions
        as a chronologically-sorted generator'''
        files = glob.glob(os.path.join(dirname, globPattern))
        return self.parseFiles(files, startT=startT, endT=endT)

    def parseFiles(self, files, startT=None, endT=None):
        ''' Parse a list of files, returning results that are 
        chronologically sorted through a generator '''
        for f in sorted(files):
            count = 0
            for obj in self.parseFile(f, startT=startT, endT=endT):
                count += 1
                yield obj

    def parseFile(self, filename, startT=None, endT=None):
        ''' Parse a single file '''
        fileStartT = NGtime.NGtimestamp.fromDsLogFilename(filename)
        # seconds left in the current hour
        secondsLeft = 3600.0 - fileStartT.minute*60 - fileStartT.second \
                - fileStartT.microsecond/1.0e6
        fileEndT = fileStartT + datetime.timedelta(seconds=secondsLeft)

        if startT is not None and fileEndT < startT:
            # This file ends before the start time, so skip it
            raise StopIteration()
        
        if endT is not None and fileStartT > endT:
            # This file starts after the end time, so skip it
            raise StopIteration()

        # This file (might) include data we're interested in,
        # so we have to read the whole file to find it
        for line in open(filename, 'r'):

            parts = line.strip().split('\t')
            dslogTime = NGtime.NGtimestamp.fromDsLog(parts[1])

            if startT is not None and dslogTime < startT:
                # sample is before start time
                continue

            if endT is not None and dslogTime > endT:
                # sample is after end, stop reading entire file
                raise StopIteration()

            ret = {'src': parts[0], 'sensor': parts[2], 'dslog': dslogTime}
            ret = self.parseLine(parts[3], ret=ret)
            if ret:
                yield ret
    
    def parseLine(self, line, ret=None, logtime=None):
        raise ValueError('Dslog is an abstract object that can''t actually parse everything!')

    def closestObject(self, dirname, globPattern, time, filtObj=None, initTolSec=5):
        reftime = time
        delta = datetime.timedelta(seconds=initTolSec)

        bestDt = None
        bestObj = None
        for obj in self.parseDir(dirname, globPattern, startT=(reftime - delta), endT=(reftime+delta)):

            # Filter the object through the supplied function
            good = True
            try:
                if filtObj:
                    good = bool(filtObj(obj))
            except Exception as e:
                traceback.print_exc()
                print('\nUnable to filter object, rejecting')
                good = False
            if not good:
                continue

            dt = abs(obj['time'] - reftime)
            if bestDt is None or dt < bestDt:
                bestDt = dt
                bestObj = obj

        return bestObj




                


class Usbl(Dslog):
    ''' A class to parse USBL data from Tracklink that's in TP2 format '''
    def __init__(self):
        super(Usbl, self).__init__()

    def parseLine(self, line, ret=None, logtime=None):
        ''' Parse a single line for this specific sensor '''
        if ret == None:
            ret = {}

        if logtime == None and 'dslog' in ret:
           logtime = ret['dslog']

        ret['target'] = int(line[0])
        ret['timestr'] = line[2:10]
        ret['reserved'] = line[11:14]
        ret['bearing'] = float(line[15:20])
        ret['slantRange'] = float(line[21:28])
        ret['x'] = float(line[29:37])
        ret['y'] = float(line[38:46])
        ret['z'] = float(line[47:54])

        if logtime:
            # fix the time to use date from DSLOG and time
            # from the sample
            timeparts = ret['timestr'].split(':')
            secondsSinceMidnight = float(timeparts[0])*3600.0 \
                    + float(timeparts[1])*60.0 + float(timeparts[2])
            rettime = copy.deepcopy(logtime)
            rettime.applyGPStime(secondsSinceMidnight)

            ret['time'] = rettime

        return ret

class Nmea(Dslog):
    ''' A class to parse DsLog files with an NMEA payload'''
    def __init__(self):
        super(Nmea, self).__init__()
        self._nmeaparser = NGnmea.NMEAParser()

    def parseLine(self, line, ret=None, logtime=None):
        ''' Parse a single line using the NGnmea parser'''
        if ret == None:
            ret = {}

        if logtime == None and 'dslog' in ret:
           logtime = ret['dslog']

        obj = self._nmeaparser.parseLine(line)

        # apply a log time, if available
        if logtime:
            rettime = copy.deepcopy(logtime)
            if 'gps_t' in obj:
                rettime.applyGPStime(obj['gps_t'])

            ret['time'] = rettime

        ret.update(obj)
        return ret

class Json(Dslog):
    ''' A class to parse DsLog files with a JSON payload'''
    def __init__(self):
        super(Json, self).__init__()
        
    def parseLine(self, line, ret=None, logtime=None):
        ''' Parse a single line using the JSON parser'''
        if ret == None:
            ret = {}
        if logtime == None and 'dslog' in ret:
            logtime = ret['dslog']

        obj = json.loads(line)

        if logtime:
            if 'time' in obj:
                obj['json_time'] = obj['time']
            ret['time'] = copy.deepcopy(logtime)

        ret.update(obj)
        return ret


def readDive(divesfile, divenum):
    ''' Read the start/end time from Ethan's dives.tsv setup '''
    for line in open(divesfile, 'r'):
        # Skip comments
        if line[0] == '#':
            continue
        parts = line.split('\t')
        print(parts)
        if not parts:
            continue
        if parts[0] == divenum:
            ret = {'divenum': parts[0].strip(),
                    'site': parts[1].strip()}


            fields = ['inwater', 'onbottom', 'offbottom', 'ondeck']
            for idx in range(0,len(fields)):
                if len(parts) > 2+idx and parts[2+idx]:
                   tmp = NGtime.NGtimestamp.fromDsLog(parts[2+idx])
                   if tmp:
                       ret[fields[idx]] = tmp
            return ret
    raise ValueError('Could not find requested dive name!')

if __name__ == '__main__':
    import glob
    import os
    testdir = '/home/ivaughn/Desktop/usbl_test/raw'

    files = glob.glob(os.path.join(testdir, '*.USBL'))
    parser = Usbl()
    count = 0
    minTime = None
    maxTime = None
    for obj in parser.parseDir(testdir, '*.USBL', \
            startT=datetime.datetime(2014,5,8,8,47,25), \
            endT=datetime.datetime(2014,5,8,8,59,59)):
        if not minTime or minTime > obj['time']:
            minTime = obj['time']

        if not maxTime or maxTime < obj['time']:
            maxTime = obj['time']

        count += 1
    print('Got %d objects' % count)
    print('From %s to %s' % (minTime.isoformat(), maxTime.isoformat()))
    print(obj)



