# Starting the notebook

Once jupyter notebook has been installed, simply run:

```
jupyter notebook
```

See also: https://jupyter.readthedocs.io/en/latest/running.html

Then go to a browser, open `http://localhost:8888` and open the file
`usbl-eval-NA097.ipynb` on your machine.

# Setup & Imports

If jupyter notebook isn't yet installed:

sudo apt install python3-pip
python3 -m pip install --upgrade # (run this twice)
python3 -m pip install jupyter # (not available in apt, I checked)
sudo apt install python3-numpy python3-pandas python3-pyproj
